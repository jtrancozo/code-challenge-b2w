# Code Challenge B2W

Esse projeto foi feito usando o template básico react-starter com React (v16.4) e usando Webpack (v4).

---

## Desenvolvido com

* [Node.js 8.11](https://nodejs.org/en/) - Javascript runtime
* [React 16.4](https://reactjs.org/) - Biblioteca para contruir interfaces
* [Babel 6.26](https://babeljs.io/) - Transpiler para javascript
* [Webpack 4.x](https://webpack.js.org/) - Um empacotador de módulos
* [SCSS](http://sass-lang.com/) - Metalingnuagem para css

---

## Vamos lá!

Essas instruções vão permitir rodar o projeto na su máquina local tanto para desenvolvimento quanto para testes.

### Pré-requisitos

Tenha instalado os seguintes softwares.

* Node 8.x
* Npm 3.x

Digite os seguintes comandos no terminal para verificar as versões do node e do npm.

  ```bash
  node -v
  npm -v
  ```

### Install

Siga os seguintes passos para obter o projeto.

  ```bash
  git clone https://gthell1@bitbucket.org/jtrancozo/code-challenge-b2w.git
  ```

Instale os módulos

   ```bash
   cd code-challenge-b2w
   npm install
   ```

### Build

#### Build da aplicação

Para gerar a versão de produção na pasta public.

```bash
npm run build:prod
```
Outras opções

dev | prod
:---: | :---:
npm run build:dev | npm run build:prod

#### Build da aplicação e visualizar mudanças

dev | prod
:---: | :---:
npm run build:dev:watch | npm run build:prod:watch

### Run

#### Run Start

Isso vai rodar a task _'serve:dev'_ no npm.
É forma mais rápida de iniciar o projeto para desenvolvimento.


```bash
npm start
```

#### Run Dev Server

```bash
npm run serve:dev
```
#### Testes

Para rodar os testes.
```bash
npm test
```