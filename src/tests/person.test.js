import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Person from '../components/person';
//import Person from '../components/Person';

describe('Testa seção do profissional', () => {
    it("Deve renderizar o componente", () => {
        const data = {
            image: '',
            name: 'Jonathan Silva',
            profession: 'Front-end Developer'
        }
        const wrapper = render(<Person person={data} />);
        expect(wrapper.length).toEqual(1);
    });
});