import React from 'react';
import { shallow, mount, render } from 'enzyme';
//import sinon from sinon;
import App from '../components/App';
//import Person from '../components/Person';

describe('render page', () => {
    it("Deve renderizar a página", () => {
        const wrapper = render(<App />);
        expect(wrapper.length).toEqual(1);
    });
});