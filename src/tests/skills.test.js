import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Skills from '../components/skills';

const mock = [
    {
        "name": "React",
        "value": "60%"
    },
    {
        "name": "Javascript",
        "value": "80%"
    },
    {
        "name": "Css",
        "value": "90%"
    }
];

describe('Testa seção <Skills />', () => {
    it("Deve renderizar uma skill", () => {
        const item = mock.slice(0,1);
        const wrapper = render(<Skills skills={item} />);
        const section = wrapper.find('.section-skills');
        const name = wrapper.find('.section-skills .skill-name').text();
        const value = wrapper.find('.section-skills .skill-value').data('value');

        expect(section.length).toEqual(1);
        expect(name).toBe(item[0].name);
        expect(value).toBe(item[0].value);
    });
});