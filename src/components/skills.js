import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Skills extends Component {
    constructor(props) {
        super(props);
    }

    renderSkills(skill) {
        return(
            <div className="section-skills" key={skill.name}>
                <h4 className="skill-name">{skill.name}</h4>
                <div className="skill">
                    <span className="skill-value" data-value={skill.value} style={{width: skill.value}}></span>
                </div>
            </div>
        );     
    }

    render() {
        const skills = this.props.skills;
        return (
            <section className="section-skill aside-section">
                <h3 className="section-title">Skills</h3>
                {skills.map(this.renderSkills)} 
            </section>
        );
    }
}

Skills.propTypes = {
    skills: PropTypes.array
};

export default Skills;