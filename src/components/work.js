import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Work extends Component {
    constructor(props) {
        super(props);
    }

    renderWork(work) {
        return(
            <div className="work article" key={work.name}>
                <h4>{work.name}</h4>
                <h5>{work.date}</h5>
                <div className="content">
                    {work.description}
                </div>
            </div>
        );     
    }

    render() {
        const work = this.props.work;
        return (
            <section className="section-work articles">
                <h3 className="section-title">Work experience</h3>
                {work.map(this.renderWork)} 
            </section>
        );
    }
}

Work.propTypes = {
    work: PropTypes.array
};

export default Work;