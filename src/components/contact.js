import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Contact extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const contact = this.props.contact;
        return (
            <section className="section-contact aside-section">
                <h3 className="section-title">Contact</h3>

                <div className="sub-section">
                    <p className="text"><a href={'tel:' + contact.tel} className="tel">{contact.tel}</a></p>
                    <p className="text"><a href={'tel:' + contact.cel} className="tel cel">{contact.tel}</a></p>
                </div>

                <div className="sub-section">
                    <p className="text">{contact.address}</p>
                </div>

                <div className="sub-section">
                    <p className="text">{contact.website}</p>
                    <p className="text">{contact.mail}</p>
                </div>
                
            </section>
        );
    }
}

Contact.propTypes = {
    contact: PropTypes.object
};

export default Contact;