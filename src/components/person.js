import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Person extends Component {
    constructor(props) {
        super(props);
    }

    getImage(url) {
        // fix github image on blob path
        let newUrl = url;

        if (url && url.match(/blob/)) {
            newUrl = url.replace('/blob/', '/raw/');
        } else {
            newUrl = 'https://www.freeiconspng.com/uploads/no-image-icon-0.png';
        }

        return newUrl;
    }

    render() {
        const person = this.props.person;

        return (
            <section className="section-person">
                <img src={this.getImage(person.image)} alt="" className="image"/>
                <h1 className="person-name">{person.name}</h1>
                <h2 className="person-profession">{person.profession}</h2>
            </section>
        );
    }
}

Person.propTypes = {
    person: PropTypes.object
};

export default Person;