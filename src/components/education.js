import React, { Component } from 'react';
import PropTypes from 'prop-types';

class education extends Component {
    constructor(props) {
        super(props);
    }

    renderEducation(education) {
        return(
            <section className="education article" key={education.name}>
                <h4>{education.name}</h4>
                <h5>{education.date}</h5>
                <div className="content">
                    <p>{education.description}</p>
                </div>
            </section>
        );     
    }

    render() {
        const education = this.props.education;
        return (
            <article className="section-education articles">
                <h3 className="section-title">Education</h3>
                {education.map(this.renderEducation)} 
            </article>
        );
    }
}

education.propTypes = {
    education: PropTypes.array
};

export default education;