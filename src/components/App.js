import React, { Component } from 'react';
import axios from 'axios';

import Person from './person';
import Contact from './contact';
import Profile from './profile';
import Skills from './skills';
import Work from './work';
import Education from './education';

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            profile: {}
        };
    }

    componentDidMount() {
        axios.get('http://www.mocky.io/v2/5a5e38f3330000b0261923a5')
            .then(res => res.data )
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        profile: result.profile
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render() {
        const { error, isLoaded, profile } = this.state;

        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return (
                <div className='container'>
                    <div className="loading">Loading <span className="dots">.</span><span className="dots">.</span><span className="dots">.</span></div>
                </div> 
            );
        } else {
            return (
                <div className='container'>                         
                    <div className="col-aside">
                        <aside className="aside">
                            <Person person={profile} />
                            <Profile profile={profile.description} />
                            <Contact contact={profile.contact} />
                            <Skills skills={profile.skills} />
                        </aside>
                    </div>
                    <main className="main-content">
                        <Work work={profile.experience} />
                        <Education education={profile.education} />
                    </main>
                </div>
            );
            
        }
    }
}