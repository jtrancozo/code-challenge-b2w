import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Profile extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const profile = this.props.profile;
        return (
            <section className="section-profile aside-section">
                <h3 className="section-title">Profile</h3>
                <p className="text">{profile}</p>
            </section>
        );
    }
}

Profile.propTypes = {
    profile: PropTypes.string
};

export default Profile;